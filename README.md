# NBA-Alex

Proyecto para la clase de desarrollo web

<hr>

El proyecto constará de cuatro vistas: 

* Partidos del Día
![Mockup home](mockups/home.jpeg)

* Confederaciones
![Mockup confederaciones](mockups/confederaciones.jpeg)

* Clasificaciones
![Mockup clasficaciones](mockups/clasificaciones.jpeg)

* Noticias
![Mockup noticias](mockups/noticias.jpeg)